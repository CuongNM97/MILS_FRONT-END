export const languageList = [
    {
        displayName: "vietnamese",
        imageIcon: "vietnam",
        value: "vi",
        code: "VN"
    },
    {
        displayName: "english",
        imageIcon: "english",
        value: "en",
        code: "ENG"
    },
    {
        displayName: "laos",
        imageIcon: "laos",
        value: "la",
        code: "LAOS"
    },
];
